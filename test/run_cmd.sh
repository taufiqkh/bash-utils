#!/usr/bin/env bash
source ../execution/run_cmd.sh

function assert_string {
    local expected=$1
    local found=$2
    if [ "$expected" != "$found" ]; then
        echo "Test failed: Expected $expected, found $found"
    else
        echo "Test passed."
    fi
}

function assert_int {
    local expected=$1
    local found=$2
    if [ $expected -ne $found ]; then
        echo "Test failed: Expected $expected, found $found"
    else
        echo "Test passed."
    fi
}

CMDOUT=`run_cmd echo test`
STATUS=$?
assert_string "test" "$CMDOUT"
assert_int 0 $STATUS
CMDOUT=`run_cmd mkdir invalid/directory`
STATUS=$?
assert_int 1 $STATUS
