#!/usr/bin/env bash
function prompt_yn {
    local message=$1
    local returnval
    echo "$1"
    select yn in "Yes" "No"; do
        case $yn in
        Yes ) returnval=1; break;;
        No ) returnval=0; break;;
        esac
    done
    return $returnval
}
