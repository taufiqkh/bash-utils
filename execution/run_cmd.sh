#!/usr/bin/env bash
# Run a command, checking the status of that command and exiting if the
# status was not 0.

function run_cmd {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        >&2 echo "Error: returned $status from command:"
        >&2 echo "$@"
        exit 1
    fi
}
